Управление личными задачами 
=================================================
(с возможностью командной работоы)

* [Техническое задание v 1.0](/tz/django.md)
    - [Задание на разработку структуры данных (back)](/tz/db.md)



# Требования v.1
* Авторизованный доступ к системе хранения задач
* Видимость (доступность) каждой задачи ограничена автором и ответственным
* Задачи хранятся в иерархической структуре (дерево)
* Каждая задача имеет ответственного, отетственный может менятся
* Каждая задача имеет время смерти (dead-line)
* Каждая задача имеет свободное поле для описания
* Существует справочник пользователей

# Требования v.2 (Возможное направление развития системы)
* процессы - каждая задача проходит несколько состояний, согласно маршрутному листу == бизнес процесс
             где:
             - узлы бизнес процесса == статусы задачи
             - переходы между узлами == возможные действия над задачей (или ничего...)
             - результат: положение задачи в дереве задач и присвоение статуса

* проработки - последовательность вопросов и ответов от польователей сохраняются в текстовый файл
               и привязываются к задаче. Тот-же бизнес процесс
             где:
             - узлы бизнес процесса == вопросы опросника
             - переходы между узлами == ответы пользователя
             - результат: текстовый файл привязанный к задаче

* Существует справочник дружбы (ответственный по задаче может быть назначен только среди "друзей")

* Перекресные ссылки между задачами
* Привязка материалов к задаче
    - Справочник типов документов
* Маршрутные листы на неделю и на день
    - составлене короткого списка задач на день или неделю


# Требования v.3 (Возможное направление развития системы)
* логирование всех запросов в системе (ко всем таблицам)
* синхронизация локальной и сереврной баз данных
* Пользовательские запросы с фильтрами к базе, т.е. возможность настраивать список задач на главной странице

# Требования v.4 (Возможное направление развития системы)
* Карточка задачи имеет тип (класс) документа
* Класс документа не редактируется, а задается соответствущей ссылкой
* Форма редактирования карточки задачи и любых других классов документов одна и таже
* Карточка задачи содержит списки привязанных объектов, сгруппированных по классам

# Замечания 
Я бы добавил ещё одно требование :
Таблица для хранения требований

Или в таблицу задач добавить поле тип документа, и в этой таблице хранить и требования и задачи

Далее на следующих ближайших шагах я бы обязательно написал 
- логгирование всех запросов ко всем таблицам
- синхронизацию локальной и северной бд

    
                                          * * * 

